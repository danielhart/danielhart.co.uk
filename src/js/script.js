var konami = new KonamiCode();

konami.listen(function () {
    if(navigator.platform.indexOf('Mac') > -1)
        window.location.replace("gifts/Daniel Hart.dmg");
    else if(navigator.platform.indexOf('Win') > -1)
        window.location.replace("gifts/Daniel Hart.zip");
    else
        alert('Lucky you! You get nothing!');
});